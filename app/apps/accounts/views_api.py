from django.contrib.auth import authenticate, login, logout
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.exceptions import AuthenticationFailed, ValidationError

from .serializers import RegistrationSerializer


class RegistrationAPIView(CreateAPIView):
    serializer_class = RegistrationSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            username = serializer.validated_data.get("username")
            password = serializer.validated_data.get("password1")
            user = serializer.save()
            user = authenticate(username=username, password=password)
            login(request, user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class LoginUserAPIView(APIView):
    @method_decorator(ensure_csrf_cookie)
    def post(self, request):
        username = request.data.get("username", "")
        password = request.data.get("password", "")
        if username == "" or password == "":
            raise ValidationError("incorrect parameters")
        user = authenticate(username=username, password=password)
        if user is None:
            raise AuthenticationFailed()
        login(request, user)
        return Response()


class LogoutUserAPIView(APIView):
    def post(self, request):
        logout(request)
        return Response()
