from django.views.generic import CreateView
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login

from .forms import RegistrationForm


class RegistrationView(CreateView):
    form_class = RegistrationForm
    template_name = "registration/register.html"

    def get_success_url(self):
        return redirect("accounts_login")

    def form_valid(self, form):
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password1")
        user = form.save()
        user = authenticate(username=username, password=password)
        login(self.request, user)
        return redirect("main")

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect("accounts_login")
        else:
            return super().dispatch(request, *args, **kwargs)


# class LoginUserView(View):
#     def post(self, request):
#         username = request.POST.get("username", "")
#         password = request.POST.get("password", "")

#         if username == "" or password == "":
#             return HttpResponseBadRequest("Incorrect parameters")

#         user = authenticate(request, username=username, password=password)
#         if user is None:
#             return HttpResponseBadRequest("Authentication failed")

#         login(request, user)
#         return HttpResponse()


# class LogoutUserView(View):
#     def post(self, request):
#         logout(request)
#         return HttpResponse()
