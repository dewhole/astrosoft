from django.test import TestCase
from django.contrib.auth import get_user_model


User = get_user_model()


class UserLoginTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="test_login", password="test")

    def tearDown(self):
        self.user.delete()

    def test_login_user(self):
        response = self.client.post("/accounts/api/login", {"username": "test_login", "password": "test1"})
        self.assertEqual(response.status_code, 403)

        response = self.client.post("/accounts/api/login", {"username": "test_login", "password": "test"})
        self.assertEqual(response.status_code, 200)

        response = self.client.post("/accounts/api/logout", {})
        self.assertEqual(response.status_code, 200)
