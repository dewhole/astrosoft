from django.urls import path, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from .views import main

import debug_toolbar

schema_view = get_schema_view(
    openapi.Info(
        title="InData API",
        default_version="v1",
        description="",
        # terms_of_service="https://www.google.com/policies/terms/",
        # contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path("swagger/", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui"),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
    path("admin/", admin.site.urls),
    path("", main, name="main"),
    path("accounts/", include("apps.accounts.urls")),
    path("logs/", include("apps.logs.urls")),
    path("api/accounts/", include("apps.accounts.urls_api")),
    path("api/logs/", include("apps.logs.urls_api")),
]

urlpatterns += i18n_patterns(path("i18n/", include("django.conf.urls.i18n")))
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static("templates/", document_root=settings.BASE_DIR / "templates/")

if settings.DEBUG:
    urlpatterns.append(path("__debug__/", include(debug_toolbar.urls)))
