from datetime import datetime

from django.conf import settings
from django.contrib.auth import get_user_model

from .models import Log

User = get_user_model()


def log(ip: str, user_agent: str, accept_language: str):
    if settings.LOGGER_BACKEND == "DB":
        Log.objects.create(ip=ip, user_agent=user_agent, accept_language=accept_language)
    elif settings.LOGGER_BACKEND == "PRINT":
        print(datetime.now(), ip, user_agent, accept_language)
