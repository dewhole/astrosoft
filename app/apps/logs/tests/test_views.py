from django.test import TestCase
from django.contrib.auth import get_user_model

from ..models import Log

User = get_user_model()


class LogViewsTest(TestCase):
    def setUp(self):
        self.log = Log.objects.create(ip='127.0.0.1', user_agent="Mozilla/5.0", accept_language="us")

    def tearDown(self):
        self.log.delete()

    def test_logs_view(self):
        response = self.client.get("/api/logs/data/")
        self.assertEqual(response.status_code, 200)
