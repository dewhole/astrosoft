from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model

User = get_user_model()


class Log(models.Model):
    ip = models.CharField(_("Ip"), max_length=15, blank=True, null=True)
    user_agent = models.CharField(_("User agent"), max_length=200)
    accept_language = models.CharField(_("Accept language"), max_length=30)
    datetime = models.DateTimeField(_("Date and time"), auto_now_add=True)

    class Meta:
        verbose_name = _("Log")
        verbose_name_plural = _("Logs")
        ordering = ["-datetime"]
