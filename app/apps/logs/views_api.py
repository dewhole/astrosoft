from rest_framework.views import APIView
from rest_framework.response import Response

import time

from .utils import log


class LogsAPIView(APIView):
    def get(self, request, format=None):
        accept_language = request.META.get('HTTP_ACCEPT_LANGUAGE')
        user_agent = request.META.get('HTTP_USER_AGENT')
        ip = request.META.get('REMOTE_ADDR')
        print(accept_language, user_agent, ip)
        print(accept_language, user_agent, ip)
        print(accept_language, user_agent, ip)
        print(accept_language, user_agent, ip)
        print(accept_language, user_agent, ip)
        print(accept_language, user_agent, ip)
        print(accept_language, user_agent, ip)
        print(accept_language, user_agent, ip)
        print(accept_language, user_agent, ip)
        print(accept_language, user_agent, ip)
        print(accept_language, user_agent, ip)
        print(accept_language, user_agent, ip)
        print(accept_language, user_agent, ip)
        print(accept_language, user_agent, ip)
        log(ip, user_agent, accept_language)
        current_timestamp_ms = int(time.time() * 1000)
        data = {'ts': current_timestamp_ms} 
        return Response(data)