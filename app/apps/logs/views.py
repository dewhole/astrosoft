from django.views.generic import ListView

from .models import Log


class LogsView(ListView):
    """
    Shows a list of filtered logs from :model:`logs.Logs` :filter:`LogsFilter`.

    **Context**

    ``object_list``
        Filtered instances of :model:`logs.Logs`.
    ``filter``
        Filter instances of :filter:`filter.LogsFilter`.

    **Template:**

    :template:`logs/log_list`
    """

    model = Log
    filter = None
    paginate_by = 50

    def get_queryset(self):
        if str(self.request.user) != "AnonymousUser":
            queryset = Log.objects.filter(user=self.request.user) | Log.objects.filter(user=None)
        else:
            queryset = Log.objects.filter(user=None)
        self.filter = self.filterset_class(self.request.GET, queryset=queryset)
        return self.filter.qs.distinct()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["filter"] = self.filter
        return context
