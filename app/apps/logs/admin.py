from django.contrib import admin
from django.utils.html import format_html

from .models import Log


@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    list_display = ["id", "datetime", "user_agent", "flag", "ip"]

    def flag(self, obj):
        accept_language = obj.accept_language
        country_code = accept_language.split(',')[0].split('-')[1].lower()
        flag_html = f'<img src="../../../static/flags/{country_code}.svg" alt="{country_code}" width="24" height="16">'
        return format_html(flag_html)
    

