from celery.schedules import crontab

from ..env import env
from ..project import TIME_ZONE


REDIS_HOST = env.str("REDIS_HOST")
REDIS_PORT = 6379
BROKER_TRANSPORT_OPTIONS = {"visibility_timeout": 3600}

CELERY_TIMEZONE = TIME_ZONE
CELERY_BROKER_URL = f"redis://{REDIS_HOST}:{REDIS_PORT}"
CELERY_RESULT_BACKEND = f"redis://{REDIS_HOST}:{REDIS_PORT}"
CELERYBEAT_SCHEDULER = "django_celery_beat.schedulers.DatabaseScheduler"
