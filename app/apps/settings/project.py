import os

from .env import env
from .django import BASE_DIR


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.str("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.bool("DEBUG", default=False)

# turn on logging of sql queries
if DEBUG:
    import logging

    lg = logging.getLogger("django.db.backends")
    lg.setLevel(logging.DEBUG)
    lg.addHandler(logging.StreamHandler())

# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}


CSRF_TRUSTED_ORIGINS = env.list("CSRF_TRUSTED_ORIGINS")

ALLOWED_HOSTS = env.list("ALLOWED_HOSTS")

TIME_ZONE = "Europe/Moscow"
LANGUAGE_CODE = "ru-ru"

INTERNAL_IPS = [
    # ...
    "127.0.0.1",
    # ...
]

TEST_RUNNER = env.str("TEST_RUNNER", default="django.test.runner.DiscoverRunner")
