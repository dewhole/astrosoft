# Indata main app


## Установка и запуск проекта

- `python3 -m venv .venv` - создать виртуальное окружение
- `venv/Scripts/activate` - войти в виртуальное окружение
- `pip install -r requirements.txt` - установить зависимости
- `pre-commit install` - установка pre-commit хуков для запуска линтеров перед коммитом
- `docker-compose --file docker-compose-local.yml up -d` - поднять базу данных PostgreSQL (если Вы не используете
Docker, установите PostgreSQL с официального сайта)
- `python manage.py migrate` - применить миграции к базе данных
- `python manage.py runserver` - запуск сервера для разработки
- `cd app && celery -A apps worker --beat -l INFO -Q default --scheduler django_celery_beat.schedulers:DatabaseScheduler`
запускает воркеры celery

## Если не установлен postgres (ubuntu)

- `sudo apt update`
- `sudo apt install postgresql postgresql-contrib` - установка postgres
- `sudo -u postgres psql` - cmd postgres 
- `createuser --interactive` / `sudo -u postgres createuser --interactive`- создание юзера
- `ALTER ROLE username WITH PASSWORD 'password';` - изменение пароля
- `CREATE DATABASE databasename;` - создание базы данных

## Если при установке зависимостей ошибка с libpq

- `sudo apt-get install libpq-dev`

## Заметки
- Celery настроен на запуск в часовой зоне проекта


## I18N & L10N
- документация и README на русском
- все комментарии в коде пишутся на английском
- все verbose_name и подобное пишутся на английском переводимым
- все тексты в шаблонах пишутся на английском переводимым
- `python3 manage.py makemessages -l ru` (создать и заполнить файл переводов)
- `python3 manage.py compilemessages` (скомпилировать переводы чтоб они работали)


## Тестирование и линтинг обязательно:
- `cd app`
- `black . --check --diff`
- `flake8 --max-line-length=120`
- `python3 manage.py test -v=2` запуск тестов
- `coverage run manage.py test` запуск тестов и создание анализа покрытия
- `coverage report` просмотр результататов в консоли
### Дополнительно для удобства можно:
- `coverage html` создание удобных и подробных отчетов в виде HTML
- `find . -name "*,cover" -exec rm -rf {} \;` подчистить артефакты и кэш покрытия
### Включение прекоммита:
- `pre-commit install`

# APP_NAME
## Описание
APP_NAME_DESCRIPTION_EXAMPLE

## Установка и запуск
APP_NAME_DESCRIPTION_EXAMPLE


# Периодические задачи

## periodic_task_name
Periodic_task_description


# .env переменные

## LOGS (DB OR PRINT)
LOGGER_BACKEND - Логирование в базу данных или в терминал. Принимает DB или PRINT
