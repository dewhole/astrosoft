#!/bin/bash

# compilemessages
# this breaks if run with an empty django.mo
#echo "compilemessages"
#python3 manage.py compilemessages --settings=apps.settings

# Collect static files
echo "Collect static files"
python3 manage.py collectstatic --settings=apps.settings --noinput

# Apply database migrations
echo "Apply database migrations"
python3 manage.py migrate --settings=apps.settings

echo "RUN RUN!"
gunicorn --workers=3 --bind :8000 apps.wsgi:application

#exec "$@"
